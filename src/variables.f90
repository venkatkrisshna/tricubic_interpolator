! =============================== !
!      TRICUBIC INTERPOLATOR      !
! =============================== !
!     Author: Venkata Krisshna    !
!      This file is part of a     !
!       tricubic interpolator     !
! ------------------------------- !
!    This module declares some    !
!     variables used in the       !
!          interpolator           !
! ------------------------------- !

module variables
  implicit none

  ! Precision
  integer, parameter :: SP = kind(1.0)
  integer, parameter :: WP = kind(1.0d0)
  
  ! Grid
  integer :: nx,ny,nz
  real(WP), dimension(:), allocatable ::   x,  y,  z
  real(WP), dimension(:), allocatable ::  dx, dy, dz
  real(WP), dimension(:), allocatable :: ddx,ddy,ddz

  ! Data
  real(WP), dimension(:,:,:), allocatable :: data

  ! Solution
  character(len=100) :: points_dist
  integer :: inx,iny,inz
  real(WP), dimension(:), allocatable :: ix,iy,iz
  real(WP), dimension(:,:,:), allocatable :: sol
  
end module variables
