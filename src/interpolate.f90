! =============================== !
!      TRICUBIC INTERPOLATOR      !
! =============================== !
!     Author: Venkata Krisshna    !
!      This file is part of a     !
!       tricubic interpolator     !
! ------------------------------- !
!       This module performs      !
!      tricubic interpolation     !
! ------------------------------- !

module interpolate
  use variables
  implicit none

contains

  ! ---------------------------------------------------------------------
  ! Finite difference functions for derivatives in tricubic interpolation
  
  function fx(i,j,k)
    implicit none
    integer, intent(in) :: i,j,k
    real(WP) :: fx
    fx = dx(i)*(data(i+1,j,k)-(ddx(i)**2)*data(i-1,j,k)-(1.0_WP-ddx(i)**2)*data(i,j,k))/(dx(i)*(1.0_WP+ddx(i)))
  end function fx

  function fy(i,j,k)
    implicit none
    integer, intent(in) :: i,j,k
    real(WP) :: fy
    fy = dy(j)*(data(i,j+1,k)-(ddy(j)**2)*data(i,j-1,k)-(1.0_WP-ddy(j)**2)*data(i,j,k))/(dy(j)*(1.0_WP+ddy(j)))
  end function fy

  function fz(i,j,k)
    implicit none
    integer, intent(in) :: i,j,k
    real(WP) :: fz
    fz = dz(k)*(data(i,j,k+1)-(ddz(k)**2)*data(i,j,k-1)-(1.0_WP-ddz(k)**2)*data(i,j,k))/(dz(k)*(1.0_WP+ddz(k)))
  end function fz

  function fxy(i,j,k)
    implicit none
    integer, intent(in) :: i,j,k
    real(WP) :: fxy
    fxy = dx(i)*dy(j)*(fx(i,j+1,k)-(ddy(j)**2)*fx(i,j-1,k)-(1.0_WP-ddy(j)**2)*fx(i,j,k))/(dy(j)*(1.0_WP+ddy(j)))
  end function fxy

  function fxz(i,j,k)
    implicit none
    integer, intent(in) :: i,j,k
    real(WP) :: fxz
    fxz = dx(i)*dz(k)*(fx(i,j,k+1)-(ddz(k)**2)*fx(i,j,k-1)-(1.0_WP-ddz(k)**2)*fx(i,j,k))/(dz(k)*(1.0_WP+ddz(k)))
  end function fxz

  function fyz(i,j,k)
    implicit none
    integer, intent(in) :: i,j,k
    real(WP) :: fyz
    fyz = dy(j)*dz(k)*(fy(i,j,k+1)-(ddz(k)**2)*fy(i,j,k-1)-(1.0_WP-ddz(k)**2)*fy(i,j,k))/(dz(k)*(1.0_WP+ddz(k)))
  end function fyz

  function fxyz(i,j,k)
    implicit none
    integer, intent(in) :: i,j,k
    real(WP) :: fxyz
    fxyz = dx(i)*dy(j)*dz(k)*(fxy(i,j,k+1)-(ddz(k)**2)*fxy(i,j,k-1)-(1.0_WP-ddz(k)**2)*fxy(i,j,k))/(dz(k)*(1.0_WP+ddz(k)))
  end function fxyz

  ! ---------------------------------------------------------------------

end module interpolate

! ========================= !
!  Initiating interpolator  !
! ========================= !
subroutine interpolate_init
  use interpolate
  implicit none

  ! Read input file
  call interpolate_init_input
  
  ! Read data file
  call interpolate_init_data

  ! Compute grid sizes
  call grid_d_dd
  
  return
end subroutine interpolate_init

! =================================== !
!  Performing tricubic interpolation  !
! =================================== !
subroutine interpolate_step
  use lookup
  use interpolate
  implicit none
  integer :: i,j,k
  integer :: ii,jj,kk,nn
  integer :: myi, myj, myk
  real(WP) :: xp,yp,zp
  real(WP), dimension(:), allocatable :: a,b
  
  ! Allocate arrays
  allocate(sol(1:inx,1:iny,1:inz)); sol=0.0_WP
 
  allocate(a(1:64)); a=0.0_WP
  allocate(b(1:64)); b=0.0_WP

  ! Loop over all points to be solved
  do k=1,inz
     do j=1,iny
        do i=1,inx
           
           ! Find nearest points in x, y and z
           do ii=1,nx
              if (x(ii)+epsilon(1.0_WP).gt.ix(i)) exit
           end do
           do jj=1,ny
              if (y(jj)+epsilon(1.0_WP).gt.iy(j)) exit
           end do
           do kk=1,nz
              if (z(kk)+epsilon(1.0_WP).gt.iz(k)) exit
           end do
           myi=ii-1; myj=jj-1; myk=kk-1;

           ! Check if sufficient data points around this point
           if (myi.lt.2 .or. myi.gt.nx-2) call die('Your point is dangerously close to the x-boundary!')
           if (myj.lt.2 .or. myj.gt.ny-2) call die('Your point is dangerously close to the y-boundary!')
           if (myk.lt.2 .or. myk.gt.nz-2) call die('Your point is dangerously close to the z-boundary!')
           
           ! Populate b matrix
           b=0.0_WP
           do nn=1,64
              if (nn.ge.1 .and.nn.le.8 ) b(nn)=data(myi+sten(nn   ,1),myj+sten(nn   ,2),myk+sten(nn   ,3))
              if (nn.ge.9 .and.nn.le.16) b(nn)=  fx(myi+sten(nn-8 ,1),myj+sten(nn-8 ,2),myk+sten(nn-8 ,3))
              if (nn.ge.17.and.nn.le.24) b(nn)=  fy(myi+sten(nn-16,1),myj+sten(nn-16,2),myk+sten(nn-16,3))
              if (nn.ge.25.and.nn.le.32) b(nn)=  fz(myi+sten(nn-24,1),myj+sten(nn-24,2),myk+sten(nn-24,3))
              if (nn.ge.33.and.nn.le.40) b(nn)= fxy(myi+sten(nn-32,1),myj+sten(nn-32,2),myk+sten(nn-32,3))
              if (nn.ge.41.and.nn.le.48) b(nn)= fxz(myi+sten(nn-40,1),myj+sten(nn-40,2),myk+sten(nn-40,3))
              if (nn.ge.49.and.nn.le.56) b(nn)= fyz(myi+sten(nn-48,1),myj+sten(nn-48,2),myk+sten(nn-48,3))
              if (nn.ge.57.and.nn.le.64) b(nn)=fxyz(myi+sten(nn-56,1),myj+sten(nn-56,2),myk+sten(nn-56,3))
           end do
           
           ! Compute a using inverse B operator
           a = 0.0_WP
           do jj=1,64
              do ii=1,64
                 a(jj) = a(jj) + Bi(jj,ii)*b(ii)
              end do
           end do
           
           ! Perform tricubic interpolation
           xp = (ix(i)-x(myi))/(x(myi+1)-x(myi))
           yp = (iy(j)-y(myj))/(y(myj+1)-y(myj))
           zp = (iz(k)-z(myk))/(z(myk+1)-z(myk))
           sol(i,j,k) = 0.0_WP
           do kk=1,4
              do jj=1,4
                 do ii=1,4
                    sol(i,j,k) = sol(i,j,k) + &
                         a(1+(ii-1)+4*(jj-1)+16*(kk-1))*(xp**(ii-1))*(yp**(jj-1))*(zp**(kk-1))
                 end do
              end do
           end do
           
        end do
     end do
  end do

  return
end subroutine interpolate_step

! ==================== !
!  Reading input file  !
! ==================== !
subroutine interpolate_init_input
  use io
  use interpolate
  implicit none
  real(WP) :: d1,d2,tmp,base
  integer :: i,j,k
  logical :: yeqx,zeqx
  
  ! Grid sizes
  call read_input('Number of points in x',nx)
  call read_input('Number of points in y',ny)
  call read_input('Number of points in z',nz)

  ! Allocate grid arrays
  allocate(x(1:nx)); x=0.0_WP
  allocate(y(1:ny)); y=0.0_WP
  allocate(z(1:nz)); z=0.0_WP
  
  ! Read grids
  call read_input('x grid',x)
  call read_input('y grid',y)
  call read_input('z grid',z)
  
  ! Solution points
  call read_input('Points distribution',points_dist)
  select case(trim(points_dist))
  case('single')
     inx=1
     iny=1
     inz=1
     allocate(ix(1:1)); ix=0.0_WP
     allocate(iy(1:1)); iy=0.0_WP
     allocate(iz(1:1)); iz=0.0_WP
     call read_input('x points',ix)
     call read_input('y points',iy)
     call read_input('z points',iz)
     
  case('multiple')
     call read_input('Number of required points in x',inx)
     call read_input('Number of required points in y',iny)
     call read_input('Number of required points in z',inz)
     allocate(ix(1:inx)); ix=0.0_WP
     allocate(iy(1:iny)); iy=0.0_WP
     allocate(iz(1:inz)); iz=0.0_WP
     call read_input('x points',ix)
     call read_input('y points',iy)
     call read_input('z points',iz)
     
  case('linear')
     call read_input('x start',d1)
     call read_input('x end',d2)
     call read_input('x points',inx)
     allocate(ix(1:inx)); ix=0.0_WP
     tmp=(d2-d1)/(inx-1)
     do i=1,inx
        ix(i) = real(i-1,WP)*tmp
     end do
     ix=ix+d1
     call read_input('Same in y',yeqx,.false.)
     if (yeqx) then
        iny=inx
        allocate(iy(1:iny)); iy=ix
     else
        call read_input('y start',d1)
        call read_input('y end',d2)
        call read_input('y points',iny)
        allocate(iy(1:iny)); iy=0.0_WP
        tmp=(d2-d1)/(iny-1)
        do j=1,iny
           iy(j) = real(j-1,WP)*tmp
        end do
        iy=iy+d1
     end if
     call read_input('Same in z',zeqx,.false.)
     if (zeqx) then
        inz=inx
        allocate(iz(1:inz)); iz=ix
     else
        call read_input('z start',d1)
        call read_input('z end',d2)
        call read_input('z points',inz)
        allocate(iz(1:inz)); iz=0.0_WP
        tmp=(d2-d1)/(inz-1)
        do k=1,inz
           iz(k) = real(k-1,WP)*tmp
        end do
        iz=iz+d1
     end if
     
  case('log')
     call read_input('Log base',base,10.0_WP)
     call read_input('x start',d1)
     call read_input('x end',d2)
     call read_input('x points',inx)
     allocate(ix(1:inx)); ix=0.0_WP
     tmp=((log(d2)-log(d1))/log(base))/(inx-1)
     do i=1,inx
        ix(i)=real(i,WP)*tmp+(log(d1)/log(base))
     end do
     ix=base**ix
     call read_input('Same in y',yeqx,.false.)
     if (yeqx) then
        allocate(iy(1:inx)); iy=ix
     else
        call read_input('y start',d1)
        call read_input('y end',d2)
        call read_input('y points',iny)
        allocate(iy(1:iny)); iy=0.0_WP
        tmp=((log(d2)-log(d1))/log(base))/(iny-1)
        do j=1,iny
           iy(j)=real(j,WP)*tmp+(log(d1)/log(base))
        end do
        iy=base**iy
     end if
     call read_input('Same in z',zeqx,.false.)
     if (zeqx) then
        allocate(iz(1:inx)); iz=ix
     else
        call read_input('z start',d1)
        call read_input('z end',d2)
        call read_input('z points',inz)
        allocate(iz(1:inz)); iz=0.0_WP
        tmp=((log(d2)-log(d1))/log(base))/(inz-1)
        do k=1,inz
           iz(k)=real(k,WP)*tmp+(log(d1)/log(base))
        end do
        iz=base**iz
     end if
     
  case default
     call die('Unrecognized points distribution!')

  end select
  
  return
end subroutine interpolate_init_input

! =================== !
!  Reading data file  !
! =================== !
subroutine interpolate_init_data
  use io
  use interpolate
  implicit none
  character(len=100) :: myloc,filename

  ! Allocate data array
  allocate(data(1:nx,1:ny,1:nz)); data=0.0_WP

  ! Read data file
  call getcwd(myloc)
  call read_input('Data file to read',filename,'data.dat')
  filename = trim(myloc) // '/' // trim(filename)
  open(unit=21, file=filename, form='formatted')
  read(21,*) data
  close(21)

  ! Transpose data array
  ! call transpose_array

  return
end subroutine interpolate_init_data

! ==================== !
!  Transpose an array  !
! ==================== !
subroutine transpose_array
  use interpolate
  implicit none
  integer :: i,j,k
  real(WP), dimension(:,:,:), allocatable :: tmp

  allocate(tmp(1:nx,1:ny,1:nz)); tmp=0.0_WP
  do k=1,nz
     do j=1,ny
        do i=1,nx
           tmp(j,i,k) = data(i,j,k)
        end do
     end do
  end do
  data=tmp
  deallocate(tmp)

  return
end subroutine transpose_array

! ==================== !
!  Compute grid sizes  !
! ==================== !
subroutine grid_d_dd
  use variables
  implicit none
  integer :: i,j,k

  ! Allocate arrays
  allocate(dx(1:nx-1)); dx=0.0_WP
  allocate(dy(1:ny-1)); dy=0.0_WP
  allocate(dz(1:nz-1)); dz=0.0_WP
  allocate(ddx(1:nx-1)); ddx=0.0_WP
  allocate(ddy(1:ny-1)); ddy=0.0_WP
  allocate(ddz(1:nz-1)); ddz=0.0_WP

  ! Compute dx, dy, dz grids
  do i=1,nx-1
     dx(i) = x(i+1) - x(i)
  end do
  do j=1,ny-1
     dy(j) = y(j+1) - y(j)
  end do
  do k=1,nz-1
     dz(k) = z(k+1) - z(k)
  end do

  ! Compute grids useful for derivatives
  do i=2,nx-1
     ddx(i) = dx(i)/dx(i-1)
  end do
  do j=2,ny-1
     ddy(j) = dy(j)/dy(j-1)
  end do
  do k=2,nz-1
     ddz(k) = dz(k)/dz(k-1)
  end do

  return
end subroutine grid_d_dd
