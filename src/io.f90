! =============================== !
!      TRICUBIC INTERPOLATOR      !
! =============================== !
!     Author: Venkata Krisshna    !
!      This file is part of a     !
!       tricubic interpolator     !
! ------------------------------- !
!   This module reads the input   !
!      file and prepares the      !
!        output environment       !
! ------------------------------- !
  
module io
  implicit none

  integer :: nline
  character(len=100), dimension(:), allocatable :: labels
  character(len=200), dimension(:), allocatable :: values

  ! Define interface for reading input
  interface read_input
     module procedure read_input_int
     module procedure read_input_intarray
     module procedure read_input_real
     module procedure read_input_floatarray
     module procedure read_input_char
     module procedure read_input_chararray
     module procedure read_input_logical
  end interface read_input

contains

  ! ==================== !
  !  Read integer input  !
  ! ==================== !
  subroutine read_input_int(mylabel,myvalue,default)
    implicit none
    character(len=*), intent(in) :: mylabel
    integer, intent(out) :: myvalue
    integer, optional, intent(in) :: default
    integer :: n,ios
    
    ! Loop over input and find the requested input
    do n=1,nline
       if (trim(mylabel).eq.trim(labels(n))) then
          read(values(n),*,iostat=ios) myvalue
          if (ios == 0 ) then
             return
          else
             call die('Problem reading '//trim(mylabel)//' from input file')
          end if
       end if
    end do

    ! Didn't find matching label
    if (present(default)) then ! use default value
       myvalue=default
    else ! End simulation
       call die('Missing entry "'//trim(mylabel)//'" in input file')
    end if

    return 
  end subroutine read_input_int
  
  ! ========================== !
  !  Read integer array input  !
  ! ========================== !
  subroutine read_input_intarray(mylabel,myvalue,default)
    implicit none
    character(len=*), intent(in) :: mylabel
    integer,dimension(:),intent(out) :: myvalue
    integer, optional, intent(in) :: default
    integer :: n,ios

    ! Loop over input and find the requested input
    do n=1,nline
       if (trim(mylabel).eq.trim(labels(n))) then
          read(values(n),*,iostat=ios) myvalue(:)
          if (ios == 0 ) then
             return
          else
             call die('Problem reading '//trim(mylabel)//' from input file')
          end if
       end if
    end do

    ! Didn't find matching label
    if (present(default)) then ! use default value
       myvalue=default
    else ! End simulation
       call die('Missing entry "'//trim(mylabel)//'" in input file')
    end if

    return 
  end subroutine read_input_intarray

  ! ================= !
  !  Read real input  !
  ! ================= !
  subroutine read_input_real(mylabel,myvalue,default)
    use variables
    implicit none
    character(len=*), intent(in) :: mylabel
    real(WP), intent(out) :: myvalue
    real(WP), optional, intent(in) :: default
    integer :: n,ios

    ! Loop over input and find the requested input
    do n=1,nline
       if (trim(mylabel).eq.trim(labels(n))) then
          read(values(n),*,iostat=ios) myvalue
          if (ios == 0 ) then
             return
          else
             call die('Problem reading '//trim(mylabel)//' from input file')
          end if
       end if
    end do

    ! Didn't find matching label
    if (present(default)) then ! use default value
       myvalue=default
    else ! End simulation
       call die('Missing entry "'//trim(mylabel)//'" in input file')
    end if

    return 
  end subroutine read_input_real

  ! ======================== !
  !  Read float array input  !
  ! ======================== !
  subroutine read_input_floatarray(mylabel,myvalue,default)
    use variables
    implicit none
    character(len=*), intent(in) :: mylabel
    real(WP),dimension(:),intent(out) :: myvalue
    real(WP), optional, intent(in) :: default
    integer :: n,ios

    ! Loop over input and find the requested input
    do n=1,nline
       if (trim(mylabel).eq.trim(labels(n))) then
          read(values(n),*,iostat=ios) myvalue
          if (ios == 0 ) then
             return
          else
             call die('Problem reading '//trim(mylabel)//' from input file')
          end if
       end if
    end do

    ! Didn't find matching label
    if (present(default)) then ! use default value
       myvalue=default
    else ! End simulation
       call die('Missing entry "'//trim(mylabel)//'" in input file')
    end if

    return 
  end subroutine read_input_floatarray

  ! ====================== !
  !  Read character input  !
  ! ====================== !
  subroutine read_input_char(mylabel,myvalue,default)
    implicit none
    character(len=*), intent(in) :: mylabel
    character(len=*), intent(out) :: myvalue
    character(len=*), optional, intent(in) :: default
    integer :: n,ios

    ! Loop over input and find the requested input
    do n=1,nline
       if (trim(mylabel).eq.trim(labels(n))) then
          read(values(n),*,iostat=ios) myvalue
          if (ios == 0 ) then
             return
          else
             call die('Problem reading '//trim(mylabel)//' from input file')
          end if
       end if
    end do

    ! Didn't find matching label
    if (present(default)) then ! use default value
       myvalue=default
    else ! End simulation
       call die('Missing entry "'//trim(mylabel)//'" in input file')
    end if

    return 
  end subroutine read_input_char

  ! ============================ !
  !  Read character array input  !
  ! ============================ !
  subroutine read_input_chararray(mylabel,myvalue)
    implicit none
    character(len=*), intent(in) :: mylabel
    character(len=*), dimension(:), intent(out) :: myvalue
    character(len=200) :: buffer
    integer :: n,nn,ios,count,ibeg

    ! Loop over input and find the requested input
    do n=1,nline
       if (trim(mylabel).eq.trim(labels(n))) then
          read(values(n),'(A)',iostat=ios) buffer
          if (ios == 0 ) then
             ! Parse buffer to find entries
             count=1 ! Number of entry in array
             ibeg=1  ! Index of beginning of entry
             do nn=1,len_trim(buffer)+1
                if (buffer(nn:nn).eq.' '.or.nn.eq.len_trim(buffer)+1) then ! Reached end of entry
                   if (len_trim(buffer(ibeg:nn-1)).gt.0) then
                      myvalue(count)=buffer(ibeg:nn-1) ! Save entry
                      count=count+1  ! Update count for next entry
                   end if
                   ibeg=nn+1  ! Update beginning of entry for next entry
                end if
             end do
             return
          else
             call die('Problem reading '//trim(mylabel)//' from input file')
          end if
       end if
    end do

    ! Didn't find matching label
    call die('Missing entry "'//trim(mylabel)//'" in input file')

    return 
  end subroutine read_input_chararray

  ! ==================== !
  !  Read logical input  !
  ! ==================== !
  subroutine read_input_logical(mylabel,myvalue,default)
    implicit none
    character(len=*), intent(in) :: mylabel
    logical, intent(out) :: myvalue
    logical, optional, intent(in) :: default
    integer :: n,ios,conv

    ! Loop over input and find the requested input
    do n=1,nline
       if (trim(mylabel).eq.trim(labels(n))) then
          if (len_trim(adjustl(values(n))).eq.1) then ! 0 or 1
             read(values(n),*,iostat=ios) conv
             if (conv.eq.1) then
                myvalue=.true.
             else
                myvalue=.false.
             end if
          else
             read(values(n),*,iostat=ios) myvalue
          end if
          if (ios == 0 ) then
             return
          else
             call die('Problem reading '//trim(mylabel)//' from input file')
          end if
       end if
    end do

    ! Didn't find matching label
    if (present(default)) then ! use default value
       myvalue=default
    else ! End simulation
       call die('Missing entry "'//trim(mylabel)//'" in input file')
    end if

    return 
  end subroutine read_input_logical

  ! ============================ !
  !  Read character array input  !
  ! ============================ !
  subroutine count_input(mylabel,count)
    implicit none
    character(len=*), intent(in) :: mylabel
    integer, intent(out) :: count
    character(len=200) :: buffer
    integer :: n,nn,ios,ibeg

    ! Loop over input and find the requested input
    do n=1,nline
       if (trim(mylabel).eq.trim(labels(n))) then
          read(values(n),'(A)',iostat=ios) buffer
          if (ios == 0 ) then
             ! Parse buffer to find entries
             count=0 ! Zero counter
             ibeg=1  ! Index of beginning of entry
             do nn=1,len_trim(buffer)+1
                if (buffer(nn:nn).eq.' '.or.nn.eq.len_trim(buffer)+1) then ! Reached end of entry
                   if (len_trim(buffer(ibeg:nn-1)).gt.0) then
                      count=count+1  ! Update count
                   end if
                   ibeg=nn+1  ! Update beginning of entry for next entry
                end if
             end do
             return
          else
             call die('Problem reading '//trim(mylabel)//' from input file')
          end if
       end if
    end do

    ! Didn't find matching label
    call die('Missing entry "'//trim(mylabel)//'" in input file')

    return 
  end subroutine count_input

end module io

! ============================= !
!  Read input file into arrays  !
! ============================= !
subroutine io_init
  use io
  implicit none

  ! Input related variables
  character(len=300) :: buffer
  character(len=100), dimension(:), allocatable :: tmp_labels
  character(len=200), dimension(:), allocatable :: tmp_values
  integer :: array_size
  integer, parameter :: nadded = 20
  integer :: pos
  integer, parameter :: fh = 15
  integer :: ios = 0

  ! Allocate arrays (start with 100 lines)
  array_size=100
  allocate(labels(100))
  allocate(values(100))

  ! Open file
  open(unit=fh, file="input")

  ! Initialize line counter
  nline=0

  ! Loop over lines in file
  do while (ios == 0)
     ! ios is negative if an end of record condition is encountered or if
     ! an endfile condition was detected.  It is positive if an error was
     ! detected.  ios is zero otherwise.

     ! Read line into buffer
     read(fh, '(A)', iostat=ios) buffer

     ! Check end of file
     if (ios /= 0) exit

     ! Check for comment
     pos = scan(buffer, '!%#')
     if (pos.gt.0) cycle

     ! Split buffer into label and values at colon
     pos = scan(buffer, ':')
     if (pos.gt.0) then ! valid line (contains colon)
        nline = nline+1    
        labels(nline) = buffer(1:pos-1)
        values(nline) = buffer(pos+1:)
     end if

     ! Reallocate arrays if needed
     if (nline.eq.array_size) then
        ! Store arrays in tmp
        allocate(tmp_labels(array_size)); tmp_labels=labels
        allocate(tmp_values(array_size)); tmp_values=values
        ! Reallocate arrays
        deallocate(labels); allocate(labels(array_size+nadded))
        deallocate(values); allocate(values(array_size+nadded))
        ! Transfer values from tmp to arrays
        labels(1:array_size)=tmp_labels; deallocate(tmp_labels)
        values(1:array_size)=tmp_values; deallocate(tmp_values)
        ! Update array size
        array_size=array_size+nadded
     end if

  end do
  close(fh)

  return
end subroutine io_init

! ============================================= !
!  Initiating program output on command window  !
! ============================================= !
subroutine io_output_header
  use variables
  implicit none

  print*,''
  print*,' ================================= WELCOME TO THE TRICUBIC INTERPOLATOR ================================='
  print*,''
  print*,'                           Please read the user manual before using the program'
  print*,' --------------------------------------------------------------------------------------------------------'
  ! print*,' Place your 3 dimensional data in order k=kmin:kmax->j=jmin:jmax->i=imin:imax'
  ! print*,' [Eg: data(1,1,1) data(1,1,2) data(1,1,3) data(1,2,1) data(1,2,2) data(1,2,3) ...]'
  ! print*,' in a file named <data.dat>'
  ! print*,' In a file named <input>, insert your points of interest'
  
  return
end subroutine io_output_header

! ============================== !
!  Write solution array to file  !
! ============================== !
subroutine io_solution
  use variables
  use io
  implicit none
  character(len=100) :: myloc,filename,linebreak
  character(len=10000) :: name,buf
  integer :: j,k

  ! Open file
  call getcwd(myloc)
  call read_input('Data file to write',filename,'sol.dat')
  filename = trim(myloc) // '/' // trim(filename)
  open(unit=23, file=filename, action="write", status="replace")

  ! Write header
  write(23,*) '! =============================== !'
  write(23,*) '!      TRICUBIC INTERPOLATOR      !'
  write(23,*) '! =============================== !'
  write(23,*) '!     Author: Venkata Krisshna    !'
  write(23,*) '! ------------------------------- !'
  write(23,*) '!          SOLUTION ARRAY         !'
  write(23,*) '! ------------------------------- !'

  ! Write grid
  linebreak=''
  write(23,*) linebreak
  write(buf,*) real(ix,SP)
  name='x grid: '//trim(adjustl(buf))
  write(23,*) trim(adjustl(name))
  write(23,*) linebreak
  write(buf,*) real(iy,SP)
  name='y grid: '//trim(adjustl(buf))
  write(23,*) trim(adjustl(name))
  write(23,*) linebreak
  write(buf,*) real(iz,SP)
  name='z grid: '//trim(adjustl(buf))
  write(23,*) trim(adjustl(name))

  ! Write solution array
  do k=1,inz
     write(23,*) linebreak
     do j=1,iny
        write(23,*) real(sol(:,j,k),SP)
     end do
  end do

  ! Close file
  close(23)
  
  return
end subroutine io_solution
