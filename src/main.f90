! =============================== !
!      TRICUBIC INTERPOLATOR      !
! =============================== !
!     Author: Venkata Krisshna    !
! ------------------------------- !
!        MAIN SOLVER PROGRAM      !
! ------------------------------- !

program main
  implicit none
  
  ! Initialize the program
  call initialize

  ! Run core program
  call core
  
  ! Terminate simulation and end program
  call terminate
  
end program main

! =================== !
!  Initialize Solver  !
! =================== !
subroutine initialize
  use io
  use variables
  implicit none

  ! Initialize IO environment
  call io_init

  ! Initiate command window output
  call io_output_header

  ! Initiate interpolator
  call interpolate_init

  return
end subroutine initialize

! ============== !
!  Core Program  !
! ============== !
subroutine core
  use io
  use variables
  implicit none

  ! Perform tricubic interpolation
  call interpolate_step

  return
end subroutine core

! ================== !
!  Terminate solver  !
! ================== !
subroutine terminate
  implicit none

  ! Write solution to output file
  call io_solution
  
  print*,'                                        Interpolation Complete!'
  print*,' --------------------------------------------------------------------------------------------------------'

  return
end subroutine terminate

! ============= !
!  Kill solver  !
! ============= !
subroutine die(text)
  implicit none
  
  character(len=*), intent(in) :: text
  print*,'Program has been killed: ',trim(text)
  call exit(0)
  
  return
end subroutine die
